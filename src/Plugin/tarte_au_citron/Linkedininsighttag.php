<?php

namespace Drupal\tarte_au_citron_linkedininsighttag\Plugin\tarte_au_citron;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tarte_au_citron\ServicePluginBase;

/**
 * A Linkedininsighttag service plugin for "Tarte au citron".
 *
 * @TarteAuCitronService(
 *   id = "linkedininsighttag",
 *   title = @Translation("Linkedin insight tag")
 * )
 */
class Linkedininsighttag extends ServicePluginBase {

  /**
   * Account key field.
   *
   * @const string
   */
  public const FIELD_LINKEDIN_KEY = 'linkedininsighttag';


  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        self::FIELD_LINKEDIN_KEY => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings_form = parent::settingsForm($form, $form_state);

    $settings_form[self::FIELD_LINKEDIN_KEY] = [
      '#type' => 'textfield',
      '#title' => $this->t('Linkedin partner id'),
      '#default_value' => $this->getSetting(self::FIELD_LINKEDIN_KEY),
      '#required' => TRUE,
    ];

    return $settings_form;
  }

}
