CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
------------

This module is a dependency of the ["Tarte au Citron"](https://www.drupal.org/project/tarte_au_citron) module.
It provides configurations for the ["Linkedin insight tag"](https://business.linkedin.com/marketing-solutions/insight-tag) service, for "Tarte au citron".

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/tarte_au_citron_linkedininsighttag/

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/tarte_au_citron_linkedininsighttag/

REQUIREMENTS
------------

This module requires the drupal:tarte_au_citron module.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Once install, you will find the module in the confiugration page of the related "Tarte au citron" module :

  - Go to: Admin » Tarte au citron » Tarte au citron settings (/admin/config/tarte_au_citron/services).
  - In the "Services" part, you can enable the "Linkedin insight tag" service.
  - Enter the partner id acccording to your linkedin requirements.
